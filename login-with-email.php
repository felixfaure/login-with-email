<?php
/*
Plugin Name: Login by Email or Username
Plugin URI: #
Description: Connexion avec email ou username ;)
Author: David FELIX-FAURE
Version: 1.0
Author URI: http://www.felixfaure.fr/
*/

function dff_email_login_authenticate( $user, $username, $password ) {
	if ( is_a( $user, 'WP_User' ) )
		return $user;

	if ( !empty( $username ) && is_email( $username ) ) {
		$usernameEmail = str_replace( '&', '&amp;', stripslashes( $username ) );
		$user = get_user_by( 'email', $usernameEmail );
		if ( !empty( $user->user_login ) ) $username = $user->user_login;
	}

	return wp_authenticate_username_password( null, $username, $password );
}
remove_filter( 'authenticate', 'wp_authenticate_username_password', 20, 3 );
add_filter( 'authenticate', 'dff_email_login_authenticate', 20, 3 );

function dff_change_username_text($text){
	if(in_array($GLOBALS['pagenow'], array('wp-login.php')) && $text == 'Identifiant'){
		$text = 'Identifiant / Email';
	}
	return $text;
}
add_filter( 'gettext', 'dff_change_username_text' );
